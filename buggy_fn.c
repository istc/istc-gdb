// buggy_fn.c

#include <stdio.h>

// This function returns a pointer to an array 
// that was allocated within the function.
// Guess what happens to the array 
// when the function returns!

int *buggy_fn(int array_size){

  int index, array[array_size];
  for (index=0; index<array_size; index++){
    array[index] = index;
  }

  int *retval;
  retval = array;

  for (index=0; index<array_size; index++){
    printf("retval[index] = %d\n", retval[index]);
  }

  return retval;
}

